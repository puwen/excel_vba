Sub StyleKill()
     Dim styT As Style
     Dim intRet As Integer
     On Error Resume Next
     For Each styT In ActiveWorkbook.Styles
         If Not styT.BuiltIn Then
             If styT.Name <> "1" Then styT.Delete
         End If
     Next styT
 End Sub

 ' https://www.mrexcel.com/forum/excel-questions/508248-excel-2007-remove-all-custom-styles.html
 ' 2018/04/30